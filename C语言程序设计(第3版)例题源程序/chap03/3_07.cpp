/*【例3-7】输入10个字符，统计其中英文字母、数字字符和其他字符的个数 */

/* 统计字符，包括英文字母、数字字符和其他字符 */
#include <stdio.h>
int main (void)
{
    int digit, i, letter, other;               /* 定义三个变量分别存放统计结果 */
    char ch;                                   /* 定义一个字符变量ch */

    digit = letter = other = 0;                /* 置存放统计结果的三个变量的初值为零 */
    printf ("Enter 10 characters: ");           /* 输入提示 */
    for (i = 1; i <= 10; i++){                  /* 循环执行了10次 */
        ch = getchar();                        /* 从键盘输入一个字符，赋值给变量 ch */
        if ((ch >= 'a' && ch <= 'z' ) || ( ch >= 'A' && ch <= 'Z'))
            letter ++;                         /* 如果ch是英文字母，累加letter */
        else if (ch >= '0' && ch <= '9')
            digit ++;                          /* 如果ch是数字字符，累加digit */
        else                	
            other ++;                          /* ch是除字母、数字字符以外的其他字符，累加other */
    }
    printf( "letter=%d, digit=%d, other=%d\n", letter, digit, other);

    return 0;
}
